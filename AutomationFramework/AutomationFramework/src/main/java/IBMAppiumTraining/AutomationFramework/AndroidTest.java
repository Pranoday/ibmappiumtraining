package IBMAppiumTraining.AutomationFramework;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import IBMAppiumTraining.Utilities.Utils;
import io.appium.java_client.android.AndroidDriver;

public class AndroidTest extends MobileAutomationTest
{
	@Override
	public void StartTest()
	{
		Utils.InitialiseEnvVars("AndroidEnvVars.txt");
		DesiredCapabilities Cap=new DesiredCapabilities();
		Cap.setCapability("app",Utils.EnvVars.get("AndroidAPKFilePath") );
		Cap.setCapability("appPackage",Utils.EnvVars.get("AndroidAppPackage") );
		Cap.setCapability("appActivity", Utils.EnvVars.get("AndroidAppActivity"));
		Cap.setCapability("deviceName", Utils.EnvVars.get("AndroidDeviceName"));
		Cap.setCapability("platformName","Android" );
		Cap.setCapability("automationName", Utils.EnvVars.get("AutomationName"));
		Cap.setCapability("fullReset", true);
		try 
		{
			Driver=new AndroidDriver(new URL("http://localhost:4723/wd/hub"),Cap);
		} 
		catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			System.out.println("Appium server URL is formatted wrong");
		}
		
	}

	@Override
	public void CreateObjectRepository(String FileName)
	{
		super.CreateObjectRepository(FileName+"_Android");
	}
	@Override
	public void EnterText(WebElement E,String Txt)
	{
		E.sendKeys(Txt);
	}
}
