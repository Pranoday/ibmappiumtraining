package IBMAppiumTraining.AutomationFramework;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import IBMAppiumTraining.Utilities.Utils;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
public class IOSTest extends MobileAutomationTest
{
	@Override
	public void StartTest()
	{
		Utils.InitialiseEnvVars("IOSEnvVars.txt");
		DesiredCapabilities Cap=new DesiredCapabilities();
		Cap.setCapability("app",Utils.EnvVars.get("IOSIPAFilePath") );
		Cap.setCapability("appPackage",Utils.EnvVars.get("IOSAppPackage") );
		Cap.setCapability("appActivity", Utils.EnvVars.get("IOSAppActivity"));
		Cap.setCapability("deviceName", Utils.EnvVars.get("IOSDeviceName"));
		Cap.setCapability("platformName","IOS" );
		Cap.setCapability("automationName", Utils.EnvVars.get("AutomationName"));
		Cap.setCapability("fullReset", true);
		try 
		{
			Driver=new IOSDriver(new URL("http://localhost:4723/wd/hub"),Cap);
		} 
		catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			System.out.println("Appium server URL is formatted wrong");
		}
	
	}
	
	@Override
	public void CreateObjectRepository(String FileName)
	{
		super.CreateObjectRepository(FileName+"_IOS");
	}

	@Override
	public void EnterText(WebElement E,String Txt)
	{
		E.clear();
		E.sendKeys(Txt);
	}
}
