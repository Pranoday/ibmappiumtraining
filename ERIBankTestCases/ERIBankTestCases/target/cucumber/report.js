$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri('IBMAppiumTraining\ERIBankTestCases\LoginFunctionality.feature');
formatter.feature({
  "line": 1,
  "name": "Testing Login functionalityof ERI Bank application",
  "description": "",
  "id": "testing-login-functionalityof-eri-bank-application",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 2,
  "name": "Testing login functionality with Valid credentials",
  "description": "",
  "id": "testing-login-functionalityof-eri-bank-application;testing-login-functionality-with-valid-credentials",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 3,
  "name": "User is on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 4,
  "name": "User enters \"company\" in UserName field",
  "keyword": "When "
});
formatter.step({
  "line": 5,
  "name": "User enters \"company\" in Password field",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "User clicks on Login button",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "User should be able to login successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginSteps.OpenLoginPage()"
});
formatter.result({
  "duration": 23279607000,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.EnterUserName()"
});
formatter.result({
  "duration": 702136500,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.EnterPassword()"
});
formatter.result({
  "duration": 1105281300,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.ClickLoginButton()"
});
formatter.result({
  "duration": 538215700,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.CheckLogin()"
});
formatter.result({
  "duration": 46200,
  "status": "passed"
});
});