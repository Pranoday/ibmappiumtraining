package IBMAppiumTraining.ERIBankPages;

import IBMAppiumTraining.AutomationFramework.MobileAutomationTest;
import IBMAppiumTraining.Utilities.Utils;

public class LoginPage 
{
	
	MobileAutomationTest T;
	public LoginPage(String MobilePlatform)
	{
		T=Utils.GetAutomationTest(MobilePlatform);
		T.StartTest();
		T.CreateObjectRepository("LoginPage");
	}
	
	public void EnterUserName(String UserName)
	{
		T.EnterText(T.ObjectRepo.get("UserNameField"), UserName);
	}
	
	public void EnterPassword(String Pass)
	{
		T.EnterText(T.ObjectRepo.get("PasswordField"), Pass);
	}
	
	public void ClickLoginButton()
	{
		T.ClickElement(T.ObjectRepo.get("LoginButton"));
	}
	
}
