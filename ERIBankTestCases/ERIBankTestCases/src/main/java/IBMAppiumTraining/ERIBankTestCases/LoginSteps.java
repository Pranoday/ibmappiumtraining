package IBMAppiumTraining.ERIBankTestCases;

import IBMAppiumTraining.ERIBankPages.LoginPage;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;

public class LoginSteps 
{
	LoginPage loginpage;
	@Given("^User is on the login page$")
	public void OpenLoginPage()
	{
		loginpage=new LoginPage("Android");
	}
	
	@When("^User enters \"company\" in UserName field$")
	public void EnterUserName()
	{
		loginpage.EnterUserName("company");
	}
	
	@When("^User enters \"company\" in Password field$")
	public void EnterPassword()
	{
		loginpage.EnterPassword("company");
	}
	
	@When("^User clicks on Login button$")
	public void ClickLoginButton()
	{
		loginpage.ClickLoginButton();
	}
	@Then("^User should be able to login successfully$")
	public void CheckLogin()
	{
		
	}
}
